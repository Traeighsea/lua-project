--------------------------------------------------------------------------------
-- @class   KeyFinder
-- @brief   Class used for holding and parsing accepted string formats
-- @member  accepted_formats[table<string>]: Table of strings containing format
--            matching
-- @desc    Class used for holding and parsing accepted string formats. Can add
--            formats to the accepted_formats to parse files for specific keys
--            or format matching
local KeyFinder = {accepted_formats = {}};
function KeyFinder:new (o)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;

  --Set the default formats
  --  For pattern matching;
  --  Note: %w is alphanumberic characters
  --  Note: ^ matches beginning and $ matches the end 
  --  Note: ? means optional
  --  Note: %s is space characters

  --  Format = ABCDEABCDEABCDEABCDE
  local format_no_spaces = '^%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w%w?%s$';
  --  Format = ABCDE-ABCDE-ABCDE-ABCDE
  --  Format = ABCDE ABCDE ABCDE ABCDE
  local format_hyphen = '^%w%w%w%w%w[%-% ]%w%w%w%w%w[%-% ]%w%w%w%w%w[%-% ]%w%w%w%w%w?%s$';

  table.insert(KeyFinder.accepted_formats, format_no_spaces);
  table.insert(KeyFinder.accepted_formats, format_hyphen);

  return o;
end

--------------------------------------------------------------------------------
-- @func    addFormat
-- @brief   Adds string to the accepted formats
-- @param   format_string[string]: String to add to the accepted formats
-- @return  void
-- @desc    Function for adding more strings to the accepted_formats table
function KeyFinder:addFormat(format_string)
  table.insert(KeyFinder.accepted_formats, format_string);
end

--------------------------------------------------------------------------------
-- @func    checkFormat
-- @brief   Checks a passed in string against the accepted formats
-- @param   input_string[string]: String you want to check the format of
-- @return  The input_string if it is found within the accepted formats, or nil
-- @desc    Checks the input string against the table of pattern formats and
--            returns the string if the input string is the correct format or
--            returns nil if it is not
function KeyFinder:checkFormat(input_string)
  for index, it_format in ipairs(KeyFinder.accepted_formats) do
    if(string.match(input_string, it_format)) then
      return input_string;
    end
  end
  return nil;
end

--------------------------------------------------------------------------------
-- findKeyFromFile
-- @brief   Returns the first key that matches the format from file
-- @param   file_name[string]: Name of the file to parse
-- @return  key_text[string]: Key parsed from file or nil
-- @desc    Given a table of valid formats, the function attempts to match
--            each line with the valid formats. The function stops whenever a 
--            match is reached and returns the matched key, or whenever the end 
--            of the file is reached. In which case returns nil. 
function KeyFinder:findKeyFromFile(file_name)
  if(file_name == nil) then
    file_name = "testfile.txt";
  end

  local file = io.open(file_name);
  local found = false;
  local key_text = nil;
  local line = nil;

  if(file == nil) then
    io.write("File Not Found.");
  end

  line = file:read("*line");
  while(not found and line ~= nil) do
    if(KeyFinder:checkFormat(line, KeyFinder.accepted_formats)) then
      found = true;
      key_text = line;
    else
      line = file:read("*line");
    end
  end

  file:close();

  if(line == nil) then
    return "No Key Found.";
  end

  io.write('Matches Format: ', key_text, '\n');
  return key_text;
end

return KeyFinder;