# About

This code was created in 2018, but never pushed to a repo.

The purpose of this project was to get familiar with Lua. The command_interface is meant to be an embeddable terminal window that I could add to say a video game. Inspiration was drawn from the World of Warcraft chat window where you could issue commands or type in specific chat channels. The command_interface is intended to be the main program to run my misc Lua files and run them.

## Modules

## KeyFinder

The KeyFinder module is a simple program that holds a collection of valid key formats, for example a Windows 7 activation key, and parses a file for the first use that matches that key format. Lua has some interesting string parsing and this was an interesting way to practice these functions.

## Chess

The Chess module is going to be a simple game of chess programmed using Lua. At this moment, it's not finished but will be implemented whenever I have more time to devote to this.
