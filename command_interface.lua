KeyFinder = require ("KeyFinder");

function main()
  running = true;
  while(running) do
    io.write('----------------------------------------');
    io.write('----------------------------------------\n');
    io.write("~::");
    --note: io.read() gets a line from the user
    local input;
    input = io.read();
    if(string.match(input, "^end$")) then
      running = false;
    elseif(string.match(input, '^/')) then
      --It's a command, try to execute the command
      if(not executeCommand(input)) then
        io.write("Error.\n");
      end
    else
      io.write("[User] Says:", input, "\n");
    end
  end
end
function executeCommand(input)
  --@TODO: Fill in actual code
  --Parse command
  input = string.sub(input, 2, #input);
  local command;
  local commandArgs = {};
  for arg in string.gmatch(input, "[^%s]+") do
    table.insert(commandArgs, arg);
  end

  --Find command
  if(string.match(commandArgs[1], "commandlist")) then
      io.write("To execute commands type \"/\" followed by the command\n\n");
      io.write("commandlist\n");
      io.write("add [number] [number]\n");
      io.write("KeyFinder [filename]\n");
      io.write("end\n");
  elseif(string.match(commandArgs[1], "add")) then
    if(#commandArgs == 3) then
      io.write(add(commandArgs[2], commandArgs[3]), "\n");
    else
      io.write("Invalid number of arguments.\n");
      return false;
    end
  elseif(string.match(commandArgs[1], "KeyFinder")) then 
    if(#commandArgs == 1) then
      findKey = KeyFinder:new();
      io.write(KeyFinder.findKeyFromFile(nil), "\n");
    elseif(#commandArgs == 2) then
      findKey = KeyFinder:new();
      io.write(KeyFinder.findKeyFromFile(commandArgs[2]), "\n");
    else
      io.write("Invalid number of arguments.\n");
      return false;
    end
  else
    io.write("Command not found.\n");
    return false;
  end

  --Check type and range for arguments


  --Call command
  return true;
end

function add(lhs, rhs)
  return lhs + rhs;
end
function sub(lhs, rhs)
  return lhs - rhs;
end
function div(lhs, rhs)
  return lhs / rhs;
end
function mul(lhs, rhs)
  return lhs * rhs;
end

--Class to hold function definitions
  --function_name; String to hold name of function
  --return_type; Return data type
  --arguments_type; Array containing the type of the argument list
  --
  --Note: Will have a function library for user made Lua functions 
  --  as well as a function library for executable C++ functions.
FunctionContainer = { function_name = nil,
                      return_type = nil,
                      arguments_type = {}};
function FunctionContainer:new (o)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;
  return o;
end
function FunctionContainer:argumentsCheck (args)
  for i, n in arguments_type do
    if(typeCheck(n, arg_type)) then
      if(rangeCheck()) then
        --Then continue
      else
        return false;
      end
    else
      return false;
    end
  end
  return true;
end

function FunctionContainer:typeCheck (arg, arg_type)
  local type_int = '^%d+$';
  local type_float = '^&d+$';
  local type_double = '^%d+$';
  local type_char = '^.+$';
  local type_bool = '^[0-1]+$';
  local type_hex = '^%x+$';
  local type_unsigned_int = '^%d+$';

  --Checking with C/C++ formats
  if(string.match(arg_type, 'int')) then
    return string.match(arg, type_int);
  elseif(string.match(arg_type, 'char')) then
    return string.match(arg, type_char);
  elseif(string.match(arg_type, 'bool')) then
    return string.match(arg, type_bool);
  elseif(string.match(arg_type, 'hex')) then
    return string.match(arg, type_hex);
  elseif(string.match(arg_type, 'unsigned_int')) then
    return string.match(arg, type_unsigned_int);
  elseif(string.match(arg_type, 'float')) then
    return string.match(arg, type_float);
  elseif(string.match(arg_type, 'double')) then
    return string.match(arg, type_double);
  end
end
function FunctionContainer:rangeCheck (arg, arg_type)
  --@TODO Add function body
  return true;
end
--Class to hold collection of Functions and access for searches
FunctionLibrary = {library = {}};
function FunctionLibrary:new (o)
  o = o or {};
  setmetatable(o, self);
  self.__index = self;
  return o;
end
function FunctionLibrary:populate ()
  --Either put all function definitions in here one by one, or
  --  let functions add themselves to the function library.
  --@TODO Add functions to library
  --table.insert(library,
    --FunctionContainer:new(o){function_name = add, return_type = "int",
      --arguments_type = {"int", "int"};
  --
end
--Input a name and arguments
  --Returns a FunctionContainer if exists
function FunctionLibrary:search (name, args)
  for i, n in library do  --Iterate through library of FC's
    if(name == n.function_name and #args == #n.args) then
      if(n:typeCheck(args)) then
        return n;
      end
    end
  end
  return nil;
end
--Input a name and arguments, calls the function if found
  --Returns the return of the called function
function FunctionLibrary:execute (name, args)

end
--asdf
main();
